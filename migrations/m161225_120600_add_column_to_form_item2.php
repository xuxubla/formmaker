<?php

use yii\db\Migration;

class m161225_120600_add_column_to_form_item2 extends Migration
{
    public function up()
    {
        $this->addColumn('form_item', 'name', $this->string(64));
    }

    public function down()
    {
        $this->dropColumn('form_item', 'name');
    }
}
