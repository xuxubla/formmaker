<?php

use yii\db\Migration;

/**
 * Handles the creation of table `form`.
 */
class m161224_120211_create_form_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('form', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'action' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('form');
    }
}
