<?php

use yii\db\Migration;

/**
 * Handles the creation of table `form_item`.
 */
class m161224_120229_create_form_item_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('form_item', [
            'element_id' => $this->primaryKey(),
            'tag' => $this->string(64)->notNull(),
            'type' => $this->string(64),
            'sort' => $this->smallInteger(),
            'form_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('form_fk', 'form_item', 'form_id', 'form', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('form_item');
    }
}
