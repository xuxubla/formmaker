<?php

use yii\db\Migration;

class m161225_114710_add_column_to_form_item extends Migration
{
    public function safeUp()
    {
        $this->addColumn('form_item', 'label', $this->string(64));
        $this->addColumn('form_item', 'class', $this->string(64));
    }

    public function safeDown()
    {
        $this->dropColumn('form_item', 'label');
        $this->dropColumn('form_item', 'class');
    }
}
