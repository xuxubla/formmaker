<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Form */

$this->title = 'Обновление данных ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Формы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="form-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<div class="form-index">
    <h2>Элементы формы</h2>
    <?= Modal::widget([
        'id' => 'item-modal',
        'size' => 'modal-lg',
        'toggleButton' => [
            'label' => 'Добавить элемент',
            'tag' => 'button',
            'class' => 'btn btn-primary',
            'data-target' => '#item-modal',
            'href' => Url::toRoute(['/form-item/create']),
        ],
        'clientOptions' => false,
    ]); ?><br><br>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'id' => 'elements',
        'dataProvider' => $formItemDataProvider,
        'filterModel' => $formItemSearchModel,
        'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

            'tag',
            'type',
            'sort',
            'name',
            'label',
            'class',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        $url = Yii::$app->getUrlManager()->createUrl(['form-item/delete','element_id'=>$key]);
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'class' => 'delete-item-link',
                            'title' => 'Удалить элемент',
                            'aria-label' => 'Удалить элемент',
                            'data-method' => 'post',
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        $url = Yii::$app->getUrlManager()->createUrl(['form-item/update','element_id'=>$key]);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'class' => 'update-item-link',
                            'title' => 'Обновить элемент',
                            'aria-label' => 'Обновить элемент',
                            'data-method' => 'post',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
<?php
Modal::begin([
    'id' => 'modal-update',
    'size' => 'modal-lg',
    'header' => '',
]);
echo Html::tag('div','', [
    'id' => 'update-item',
]);
Modal::end();
$this->registerJs(<<<JS

jQuery('#item-modal').on('click', '.form-item-create', function() {
    jQuery('#formitem-form_id').val('$model->id');
});

jQuery('.form-index').on('click', '.update-item-link', function() {
    var link = jQuery(this).attr('href');
    jQuery.ajax({
        url: link,
        success: function(response) {
            jQuery('#update-item').html(response);
            jQuery('#modal-update').modal('show');
        },        
    });
    return false;
});

jQuery('.form-index').on('click', '.delete-item-link', function() {
    if (confirm('Вы уверены, что хотите удалить элемент?')) {
        var link = jQuery(this).attr('href');
        jQuery.ajax({
            url: link,
            type: 'post',
            success: function(response) {
                jQuery('#elements').yiiGridView('applyFilter');
            },        
        });
    }
    return false;
});
JS
);
$this->registerCss(<<<CSS
.form-item-create {
    margin: 20px;
}
.modal-header {
    display: none;
}
CSS
);
