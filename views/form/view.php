<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Form */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Формы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Обновить и добавить элементы'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    /* не будем отображать пустую форму */
    if (count($model->formItems) > 0) {
        /* пройдем по всем элементам и составим форму */
        $html = "<h3>Форма {$model->name}</h3>";
        foreach ($model->formItems as $element) {
            if ($element->tag == 'input' || $element->tag == 'textarea') {

                if (!empty($element->label)) {
                    $html .= Html::label($element->label);
                }

                $class = '';
                if (!empty($element->class)) {
                    $class = $element->class;
                } else {
                    if ($element->type !== 'checkbox') {
                        $class = 'form-control';
                    }
                }

                $html .= Html::tag($element->tag, '', [
                    'type' => $element->type,
                    'name' => $element->name,
                    'class' => $class,
                ]);
            } elseif ($element->tag == 'button') {

                if (!empty($element->label)) {
                    $label = $element->label;
                } else {
                    $label = 'Кнопка';
                }

                $html .= Html::tag($element->tag, $label, [
                    'type' => $element->type,
                    'name' => $element->name,
                    'class' => !empty($element->class) ? $element->class : 'btn btn-primary',
                ]);
            }
            $html .= '<br>';
        }
        echo Html::beginForm('');
        echo $html;
        echo Html::endForm();
    }
    ?>
</div>
