<?php

use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Form Maker!</h1>

        <p class="lead">Test application</p>

        <p><a class="btn btn-lg btn-success" href="<?= Url::toRoute('form/index')?>">Let's create some form</a></p>
    </div>

</div>
