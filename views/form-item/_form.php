<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FormItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-item-form">

    <?php $form = ActiveForm::begin(['id' => 'form-for-create']); ?>

    <?= $form->field($model, 'tag')->dropDownList($model->tags) ?>

    <?= $form->field($model, 'type')->dropDownList($model->types, ['prompt' => 'Выберите type...']) ?>

    <?= $form->field($model, 'sort')->textInput(['value' => empty($model->sort) ? 200 : $model->sort]) ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'label')->textInput(['value' => empty($model->label) ? 'Label' : $model->label]) ?>

    <?= $form->field($model, 'class')->textInput() ?>

    <?= $form->field($model, 'form_id')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
