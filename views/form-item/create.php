<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FormItem */

$this->title = Yii::t('app', 'Новый элемент');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Form Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php
$this->registerJs(<<<JS
jQuery('#form-for-create').on('beforeSubmit', function() {
    var form = jQuery(this);
    var data = form.serialize();
    jQuery.ajax({
        url: form.attr('action'),
        data: data,
        type: 'post',
        success: function() {
            jQuery('#item-modal').modal('hide');
            jQuery('#formitem-sort, #formitem-name, #formitem-label, #formitem-class').val('');
            jQuery('#elements').yiiGridView('applyFilter');
        },        
    });
    return false;
});
JS
);