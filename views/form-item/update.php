<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FormItem */

$this->title = 'Обновить элемент формы';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Form Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->element_id, 'url' => ['view', 'id' => $model->element_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="form-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>
<?php
$this->registerJs(<<<JS
jQuery('#form-for-update').on('beforeSubmit', function() {
    var form = jQuery(this);
    var data = form.serialize();
    jQuery.ajax({
        url: form.attr('action'),
        data: data,
        type: 'post',
        success: function() {
            jQuery('#modal-update').modal('hide');
            jQuery('#elements').yiiGridView('applyFilter');
        },        
    });
    return false;
});
JS
);

