<?php

namespace app\controllers;

use Yii;
use app\models\FormItem;
use app\models\FormItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FormItemController implements the CRUD actions for FormItem model.
 */
class FormItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FormItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FormItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FormItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FormItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FormItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FormItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $element_id
     * @return mixed
     */
    public function actionUpdate($element_id)
    {
        $model = $this->findModel($element_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FormItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $element_id
     * @return mixed
     */
    public function actionDelete($element_id)
    {
        $this->findModel($element_id)->delete();

        return;
    }

    /**
     * Finds the FormItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FormItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
