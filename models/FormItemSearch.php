<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormItem;

/**
 * FormItemSearch represents the model behind the search form about `app\models\FormItem`.
 */
class FormItemSearch extends FormItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['element_id', 'sort', 'form_id'], 'integer'],
            [['tag', 'type', 'label', 'class', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'element_id' => $this->element_id,
            'sort' => $this->sort,
            'label' => $this->label,
            'class' => $this->class,
            'name' => $this->name,
            'form_id' => isset($params['id']) ? $params['id'] : $this->form_id,
        ]);

        $query->andFilterWhere(['like', 'tag', $this->tag])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'class', $this->class]);

        return $dataProvider;
    }
}
