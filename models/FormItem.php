<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_item".
 *
 * @property integer $element_id
 * @property string $tag
 * @property string $type
 * @property integer $sort
 * @property integer $form_id
 *
 * @property Form $form
 */
class FormItem extends \yii\db\ActiveRecord
{
    /**
     * @var array Allowed tags
     */
    public $tags = [
        'input' => 'input',
        'textarea' => 'textarea',
        'button' => 'button',
    ];

    /**
     * @var array Allowed types
     */
    public $types = [
        'text' => 'text',
        'file' => 'file',
        'submit' => 'submit',
        'checkbox' => 'checkbox',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag', 'form_id', 'name'], 'required'],
            [['sort', 'form_id'], 'integer'],
            [['tag', 'type', 'label', 'class', 'name'], 'string', 'max' => 64],
            [['form_id'], 'exist', 'skipOnError' => true, 'targetClass' => Form::className(), 'targetAttribute' => ['form_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'element_id' => 'ID',
            'tag' => 'Тег',
            'type' => 'Тип',
            'sort' => 'Сортировка',
            'label' => 'label',
            'name' => 'name',
            'class' => 'class',
            'form_id' => 'Form ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['id' => 'form_id']);
    }
}
