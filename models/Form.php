<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form".
 *
 * @property integer $id
 * @property string $name
 * @property string $action
 *
 * @property FormItem[] $formItems
 */
class Form extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],
            [['action'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'action' => Yii::t('app', 'Экшн'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormItems()
    {
        return $this->hasMany(FormItem::className(), ['form_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }
}
